<?php

namespace ButtonRequest\ApiV1\APIServices;
use ButtonRequest\ApiV1\CommonFunction\FilterData; 

class DeviceShare{
    function __construct($apiToken, $baseUrl){
        $this->apiToken = $apiToken;
        $this->baseUrl=$baseUrl;
    }

    public function toMeFetch($filterArray=[]){
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/buttonDevice/deviceSharedToMe/fetch",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/json"
          ],
        ]);
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return (
            ["result"=>json_decode($response)->result, 
              "data"=>FilterData::fetchDeviceSharedToMe(json_decode($response)->data,$filterArray)
            ]
          );        
        }
    }
    public function giveUpShareeRight($case_id){
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/buttonDevice/deviceSharedToMe/delete/".$case_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/json"
          ],
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return json_decode($response);
        }
    }

    public function shareTo($device_id, $email){
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/buttonDevice/shareDevice/new/".$email."/".$device_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/json"
          ],
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return json_decode($response);
        }

    }
    public function changeShareeRight($case_id, $right){
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/buttonDevice/shareDevice/changeRight/".$case_id."/".$right,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/json"
          ],
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return json_decode($response);
        }
    }

}