<?php

namespace ButtonRequest\ApiV1\APIServices;
use ButtonRequest\ApiV1\CommonFunction\FilterData; 

class MobileAccess{
    function __construct($apiToken, $baseUrl){
        $this->apiToken = $apiToken;
        $this->baseUrl=$baseUrl;
    }

    public function fetch($filterArray=[]){
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/mobileDevice/fetch",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/json"
          ],
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return (
            ["result"=>json_decode($response)->result, 
              "data"=>FilterData::fetchMobileAccessToken(json_decode($response)->data,$filterArray)
            ]
          );
        }
    }

    public function new($nickname){
        $curl = curl_init();

        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/mobileDevice/new/".$nickname,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/json"
          ],
        ]);
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return json_decode($response);
        }
    }

    public function amendNickname($case_id, $newNickname){
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/mobileDevice/amend/".$case_id."?nickname=".$newNickname,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/json"
          ],
        ]);
         
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return json_decode($response);
        }
    }

    public function revoke($case_id){
        $curl = curl_init();
        
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/mobileDevice/revoke/".$case_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/json"
          ],
        ]);
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return json_decode($response);
        }

    }
    

}