<?php

namespace ButtonRequest\ApiV1\APIServices;
use ButtonRequest\ApiV1\CommonFunction\FilterData; 

class DeviceList{
    function __construct($apiToken, $baseUrl){
        $this->apiToken = $apiToken;
        $this->baseUrl=$baseUrl;
    }
    public function fetch($device_id=null, $filterArray=[]){
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/buttonDevice/myDevice/list/".$device_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/json"
          ],
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return (
            ["result"=>json_decode($response)->result, 
              "data"=>FilterData::fetchDeviceList(json_decode($response)->data,$filterArray)
            ]
          );
        }
    }

    public function repeatedMessage($device_id, $action){
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/buttonDevice/myDevice/repeatedMessage/".$device_id."/".$action,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/json"
          ],
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return json_decode($response);
        }
    }

    public function buttonMessageUpdate($device_id, $buttonMessageArray){
        $passData=["dataArray"=>$buttonMessageArray];
       
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/buttonDevice/myDevice/buttonMessageUpdate/updateOrNew/".$device_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => 'passData='.urlencode(json_encode($passData)),
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/x-www-form-urlencoded"
          ],
        ]);
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return json_decode($response);
        }
    }
    public function buttonMessageDelete($device_id, $buttonIdArray){
        $passData=["dataArray"=>$buttonIdArray];
       
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/buttonDevice/myDevice/buttonMessageUpdate/delete/".$device_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => 'passData='.urlencode(json_encode($passData)),
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/x-www-form-urlencoded"
          ],
        ]);
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return json_decode($response);
        }
    }

    public function newDevice($nickname){
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => $this->baseUrl."/buttonDevice/myDevice/new/".$nickname,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer ".$this->apiToken,
            "Content-Type: application/json"
          ],
        ]);
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
          return (["result"=>"error", "data"=>[]]);
        } else {
          return json_decode($response);
        }
    }
}






