<?php
namespace ButtonRequest\ApiV1;
use ButtonRequest\ApiV1\APIServices\Message;
use ButtonRequest\ApiV1\APIServices\MobileAccess;
use ButtonRequest\ApiV1\APIServices\DeviceList;
use ButtonRequest\ApiV1\APIServices\DeviceShare;


class Client{
    function __construct($apiToken){
        $this->apiBaseUrl="https://button-request.herokuapp.com/api/v1";
        $this->apiToken = $apiToken;
        $this->message= new Message($apiToken,  $this->apiBaseUrl);
        $this->mobileAccess= new MobileAccess($apiToken,  $this->apiBaseUrl);
        $this->deviceList= new DeviceList($apiToken,  $this->apiBaseUrl);
        $this->deviceShare= new DeviceShare($apiToken,  $this->apiBaseUrl);
    }
}
