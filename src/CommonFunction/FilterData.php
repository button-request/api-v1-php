<?php


namespace ButtonRequest\ApiV1\CommonFunction;


class FilterData{
    function __construct(){
        
    }
    /*[
        "msg_id"=>""
        "device_id"=>""
        "pin"=>""
        "shared_to_me"=>""
    ]*/
    public static function fetchMessages($data, $filterDict){
        $adjustedFilterDict=self::trimDictByKey($filterDict, ["msg_id","device_id","pin","shared_to_me"]);
        return self::filterArrayByEquality($data, $adjustedFilterDict);
    }
    public static function fetchDeviceList($data, $filterDict){
        $adjustedFilterDict=self::trimDictByKey($filterDict, ["status","repeated_message"]);
        return self::filterArrayByEquality($data, $adjustedFilterDict);
    }
    public static function fetchMobileAccessToken($data, $filterDict){
        $adjustedFilterDict=self::trimDictByKey($filterDict, ["case_id","deleted_from_phone"]);
        return self::filterArrayByEquality($data, $adjustedFilterDict);
    }
    public static function fetchDeviceSharedToMe($data, $filterDict){
        $adjustedFilterDict=self::trimDictByKey($filterDict, ["case_id","device_id", "owner_email", "right"]);
        return self::filterArrayByEquality($data, $adjustedFilterDict);
    }
    public static function trimDictByKey($dict, $keyAllowedArray){
        $dict1=$dict;
        foreach ($dict1 as $key => $value) {
            if (!in_array($key, $keyAllowedArray)) {
                unset($dict1[$key]);
            }
        }
        return $dict1;
    }
    public static function filterArrayByEquality($array, $conditionDict){
        $newArray=[];
        if (count($conditionDict)>0) {
            for ($i=0; $i < count($array); $i++) { 
                $item=$array[$i];
                $add=true;
                
                foreach ($conditionDict as $key => $value) {
                    if ($item->$key != $value) {
                        $add=false;
                    }
                }
                if ($add) {
                    array_push($newArray, $item);
                }
            }
        }else{
            $newArray=$array;
        }
        return $newArray;
    }
    
    
}