
# Button API V1 Library for PHP
This PHP library enables you to use the API effectively.

## Installation
To install use this library, you may install it with composer.

Open the Terminal and navigate to the folder you desire. Then type, 
```bash
composer require button-request/api-v1
```

## How to use?


To use that, you must first declare the following,
```php
use ButtonRequest\ApiV1\Client;
$apiClient=new Client("{API Token}");
```

### Message
#### Fetch

```php
$response=$apiClient->message->fetch();
```
You may also add filter conditions,
```php
$response=$apiClient->message->fetch(
    "msg_id"=>"{Msg ID}",
    "device_id"=>"{Device ID}",
    "pin"=>"{Pin Status}",
    "shared_to_me"=>"{Shared To Me}"
);
```
Eligible Filter Parameters: msg_id, device_id, pin, shared_to_me

#### Pin Status

```php
$response=$apiClient->message->pinStatus("{Message ID}", "{Pin or Unpin}");

```

#### Delete
```php
$response=$apiClient->message->delete("{Message ID}");

```

### DeviceList

#### Fetch
```php
$response=$apiClient->deviceList->fetch();

```
You may also add filter,
```php
$response=$apiClient->deviceList->fetch("{Device ID}", 
    [
        "status"=>"{Status}",
        "repeated_message"=>"{Repeated Message}",
    ]
);

```
Eligible Filter Parameters: status, repeated_message


#### Repeated Message
```php
$response=$apiClient->deviceList->repeatedMessage("{Device Id}", "{Action}");
```

#### Button Message Update
```php
$response=$apiClient->deviceList->buttonMessageUpdate("{Device Id}", 
    [
        ["buttonNo"=>"1", "message"=>"This is 1"],
        ["buttonNo"=>"2", "message"=>"This is 2"]
    ]
);

```

#### Button Message Delete
```php
$response=$apiClient->deviceList->buttonMessageDelete("{Device Id}", 
    [
        "{Button No 1}", "{Button No 2}"
    ]
);
```

#### New Device
```php
$response=$apiClient->deviceList->newDevice("{Nickname}");
```

### Device Share
#### Fetch Shared To Me
```php
$response=$apiClient->deviceShare->toMeFetch();
```
You may also pass filter array,
```php
$response=$apiClient->deviceShare->toMeFetch([
    "case_id"=>"{Case ID}",
    "device_id"=>"{Device ID}",
    "owner_email"=>"{Email}",
    "right"=>"{Right}"
]);
```
Eligible Parameter: case_id, device_id, owner_email, right

#### Give Up Sharee Right
```php
$response=$apiClient->deviceShare->giveUpShareeRight("{Case ID}");
```

#### Share To
```php
$response=$apiClient->deviceShare->shareTo("{Device ID}", "{Email}");
```


#### Change Sharee Right
```php
$response=$apiClient->deviceShare->changeShareeRight("{Case ID}", "{Right}");
```

### Mobile Access

#### Fetch
```php
$response=$apiClient->mobileAccess->fetch();

```
You may also pass filter array.

```php
$response=$apiClient->mobileAccess->fetch(
    [
        "case_id"=>"{Case ID}",
        "deleted_from_phone"=>"{Deleted or Not}"
    ]
);
```
Eligible Parameter: case_id, deleted_from_phone

#### New
```php
$response=$apiClient->mobileAccess->new("{Nickname}");

```

#### Amend Nickname


```php
$response=$apiClient->mobileAccess->amendNickname("{Case ID}", "{New Nickname}");

```
#### Revoke

```php
$response=$apiClient->mobileAccess->revoke("{CASE ID}");

```


## Response

Please refer to the API documentation